#include "class1.h"


bool class1::operator<(class1& other)
{
	if (this->num < other.num)
	{
		return true;
	}
	else
	{
		return false;
	}
}


class1& class1::operator=(class1& other)
{
	this->num = other.num;
	return *this;
}


bool class1::operator==(class1& other)
{
	return this->num == other.num;
}


std::ostream&operator<<(std::ostream & output, const class1 & print)
{
	output << print.num;
	return output;
}
