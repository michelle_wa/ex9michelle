#pragma once

template<class T>
int compare(T first, T seconed)
{
	if (first == seconed)
	{
		return 0;
	}
	else if (first < seconed)
	{
		return 1;
	}
	else
	{
		return -1;
	}	
}

template <class T>
void bubbleSort(T* arr, int size)
{
	int i = 0;
	int j = 0;
	T temp;
	for (i = 0; i < size - 1; ++i)
	{

		for (j = 0; j < size - 1; ++j)
		{
			if (compare(arr[j], arr[j + 1]) == -1)
			{
				temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}
	}
}

template<class T>
void printArray(T* arr, int size)
{
	for (int i = 0; i < size; ++i)
	{
		std::cout << arr[i] << std::endl;
	}
}

