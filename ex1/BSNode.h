#pragma once

#ifndef BSNode_H
#define BSNode_H

#include <string>
#include <iostream>

using namespace std;

class BSNode
{
private:
	string _data;
	BSNode* _left;
	BSNode* _right;

	int _count;
	int getCurrNodeDistFromInputNode(const BSNode* node) const; 

public:
	BSNode(string data);
	BSNode(const BSNode& other);

	~BSNode();

	void insert(string value);
	BSNode& operator=(const BSNode& other);

	bool isLeaf() const;
	string getData() const;
	BSNode* getLeft() const;
	BSNode* getRight() const;

	bool search(string val) const;

	int getHeight() const;
	int getDepth(const BSNode& root) const;

};

#endif
