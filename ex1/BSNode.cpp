#include "BSNode.h"


BSNode::BSNode(string data)
{
	_data = data;
	_left = nullptr;
	_right = nullptr;
	_count = 1;
}


BSNode::BSNode(const BSNode& other)
{
	_data = other._data;
	if (other._right != nullptr)
	{
		this->_right = new BSNode(*(other._right));
	}
	else
	{
		_right = nullptr;
	}
	if (other._left)
	{
		this->_left = new BSNode(*(other._left));
	}
	else
	{
		_left = nullptr;
	}
}


BSNode::~BSNode()
{
	if (this->_right)
	{
		delete this->_right;
	}
	if (this->_left)
	{
		delete this->_left;
	}
}


void BSNode::insert(string value)
{
	if (value == this->_data)
	{
		_count++;
	}
	else if (value > this->_data)
	{
		if (!this->_right)
		{
			this->_right = new BSNode(value);
		}
		else
		{
			_right->insert(value);
		}
	}
	else if (value < this->_data)
	{
		if (!this->_left)
		{
			this->_left = new BSNode(value);
		}
		else
		{
			_left->insert(value);
		}
	}
}


BSNode & BSNode::operator=(const BSNode & other)
{
	delete this;
	_data = other._data;
	_left = nullptr;
	_right = nullptr;
	cout << other._data << " : " << other._left << " " << other._right << endl;
	if (other._right)
	{
		this->_right = new BSNode(*(other._right));
	}
	if (other._left)
	{
		this->_left = new BSNode(*(other._left));
	}
	return *this;
}


bool BSNode::isLeaf() const
{
	return !(_right || _left);
}


string BSNode::getData() const
{
	return _data;
}


BSNode* BSNode::getLeft() const
{
	return _left;
}


BSNode* BSNode::getRight() const
{
	return _right;
}


bool BSNode::search(string val) const
{
	bool flag = false;
	if (val > this->_data && this->_right)
	{
		flag = this->_right->search(val);
	}
	else if (val < this->_data && this->_left)
	{
		flag = this->_left->search(val);
	}
	else if (val == this->_data)
	{
		flag = true;
	}
	return flag;
}


int BSNode::getHeight() const
{
	int rightHeight = 0;
	int leftHeight = 0;
	if (isLeaf())
	{
		return 0;
	}
	if (this->_right)
	{
		rightHeight = this->_right->getHeight() + 1;
	}
	if (this->_left)
	{
		leftHeight = this->_left->getHeight() + 1;
	}

	if (rightHeight < leftHeight)
	{
		return leftHeight;
	}
	else
	{
		return rightHeight;
	}
}


int BSNode::getDepth(const BSNode & root) const
{
	if (!root.search(this->_data))
	{
		return -1;
	}
	return getCurrNodeDistFromInputNode(&root);
}



int BSNode::getCurrNodeDistFromInputNode(const BSNode * node) const
{
	if (this->_data == node->_data)
	{
		return 0;
	}
	else if (this->_data > node->_data)
	{
		return getCurrNodeDistFromInputNode(node->_right) + 1;
	}
	else if (this->_data < node->_data)
	{
		return getCurrNodeDistFromInputNode(node->_left) + 1;
	}
	return -1;
}


